from django.shortcuts import render
from django.views.generic import RedirectView
# Create your views here.
from index.models import AppConfig


class AppView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        if self.request.user_agent.os.family.lower() == 'ios':
            return AppConfig.objects.last().appstore_link
        if self.request.user_agent.os.family.lower() == 'android':
            return AppConfig.objects.last().playmarket_link
        return AppConfig.objects.last().desktop_link

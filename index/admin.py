from django.contrib import admin

# Register your models here.
from index.models import AppConfig

admin.site.register(AppConfig)
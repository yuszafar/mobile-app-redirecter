from django.db import models


class AppConfig(models.Model):
    desktop_link = models.URLField()
    appstore_link = models.URLField()
    playmarket_link = models.URLField()